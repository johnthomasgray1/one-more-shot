from django.forms import ModelForm
from todos.models import TodoList, TodoItem

class Todo_List_Form(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]

class Item_Form(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
