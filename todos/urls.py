from django.urls import path
from todos.views import todolist, tododetail, todocreate, todoupdate, tododelete, itemcreate, itemupdate

urlpatterns = [
    path("", todolist, name="todo_list_list"),
    path("<int:id>/detail/", tododetail, name="todo_list_detail"),
    path("create/", todocreate, name="todo_list_create"),
    path("<int:id>/edit/", todoupdate, name="todo_list_update"),
    path("<int:id>/delete/", tododelete, name="todo_list_delete"),
    path("items/create/", itemcreate, name="todo_item_create"),
    path("items/<int:id>/edit/", itemupdate, name="todo_item_update")
]
