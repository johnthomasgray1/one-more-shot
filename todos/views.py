from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import Todo_List_Form, Item_Form

def todolist(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list" : todo_list_list,
    }
    return render(request, "todos/list.html", context)

def tododetail(request, id):
    todo_object = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object" : todo_object,
    }
    return render(request, "todos/detail.html", context)

def todocreate(request):
    if request.method == "POST":
        form = Todo_List_Form(request.POST)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("todo_list_detail", todo_instance.id)
    else:
        form = Todo_List_Form()

    context = {
        "form" : form
    }
    return render(request, "todos/create.html", context)

def todoupdate(request, id):
    todo_object = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        # Get the form with post request as argument
        form = Todo_List_Form(request.POST, instance=todo_object)
        # Check if the form is valid
        if form.is_valid():
            # Save an instance of the form
            todo_object = form.save()
            # return redirect
            return redirect("todo_list_detail", id=todo_object.id)
        # Else: assign the populated form
    else:
        form = Todo_List_Form(instance=todo_object)
    # create context with form in dictionary
    context = {
        "form" : form
    }
    # return render with update url and context
    return render(request, "todos/edit.html", context)

def tododelete(request, id):
    todo_object = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_object.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def itemcreate(request):

    if request.method == "POST":
        form = Item_Form(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id )
    else:
        form = Item_Form()

    context = {
        "form" : form
    }

    return render(request, "todos/itemcreate.html", context)

def itemupdate(request, id):
    item_obj = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = Item_Form(request.POST, instance=item_obj)
        if form.is_valid():
            item_obj = form.save()
            return redirect("todo_list_detail", id=item_obj.list.id)
    else:
        form = Item_Form(instance=item_obj)

    context = {
        "form" : form,
    }
    return render(request, "todos/itemedit.html", context)
